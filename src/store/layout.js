import { defineStore } from 'pinia'

export default defineStore('layout', {
    state: ()=> {
        return {
            isExpand: true, // 侧边栏是否展开
        }
    },
    getters: {

    },
    actions: {
        // 切换侧边栏
        toggleSider() {
            console.log('切换侧边栏', this.isExpand);
            this.isExpand = !this.isExpand
        }
    }
})