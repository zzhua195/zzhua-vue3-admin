import { defineStore } from 'pinia'

import { login } from '@/api/loginApi'

function retrieveLocalToken() {
    console.log('read token'); 
    return localStorage.getItem('token') || '' 
}
function clearLocalToken() {
    return localStorage.clear('token')
}

export default defineStore('user',{
    state: () => {
        return {
            token: retrieveLocalToken() // 当刷新页面时, 这个会加载一次
        }
    },
    getters: {

    },
    actions: {
        doLogin(data) {
            return new Promise((resolve, reject) => {
                login(data).then(res=>{
                    this.token = res // 同样先存入到pinia中
                    localStorage.setItem('token', res)
                    resolve(data)
                }).catch(err=>{
                    reject(err)
                })
            })
        },
        clearUserInfo() {
            this.token = null
            clearLocalToken()
        }
    }
})