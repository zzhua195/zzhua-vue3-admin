import { createApp } from 'vue'
import './style.css'
import '@/assets/iconfont/iconfont.css'
import App from './App.vue'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import Messager from '@/utils/messager'


import router from '@/router'
import pinia from '@/store'

import perm from '@/directive/perm'


const app = createApp(App)

app.config.globalProperties.Messager = Messager

app.use(pinia)
app.use(router)
app.use(ElementPlus)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

for(let key in perm) {
    app.directive(key, perm[key])
}

app.mount('#app')
