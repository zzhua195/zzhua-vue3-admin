import request from '@/utils/request'

export function getCaptchaImage()  {
    return request({
        url: 'captchaImage',
    })
}

export function login(data)  {
    return request({
        method:'post',
        url: 'user/login',
        data
    })
}

export function getMenus()  {
    return request({
        method:'get',
        url: 'test/getMenus'
    })
}

export function getRoutes()  {
    return request({
        method:'get',
        url: 'test/getRoutes'
    })
}

export function getPerms()  {
    return request({
        method:'get',
        url: 'test/getPerms'
    })
}