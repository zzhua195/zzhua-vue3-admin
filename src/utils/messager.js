import { ElMessage } from "element-plus";
const Messager = {
    ok(msg){
        ElMessage.success(msg)
    },
    error(msg) {
        ElMessage.error(msg)
    },
    warn(msg) {
        ElMessage.warning(msg)
    }
}
export default Messager