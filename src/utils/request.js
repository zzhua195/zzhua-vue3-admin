import axios from 'axios'
import Messager from './messager';
import pinia from '@/store'
import useUser from '@/store/user'



const instance = axios.create({
    baseURL: 'http://127.0.0.1:8080/api',
    timeout: 10000
})

instance.interceptors.request.use((config)=>{
    // debugger
    let userStore = useUser()
    if(userStore.token) {
        console.log('userStore.token',userStore.token);
        config.headers['Authorization'] = userStore.token
    }
    return config;
})

instance.interceptors.response.use(response=>{
    if(response.data.errno == 0) {
        return Promise.resolve(response.data.data)
    } else {
        if(response.data.errno == 501) {
            Messager.error('请重新登录')
            window.location.href = '/login'
        } else {
            Messager.error(response.data.errmsg)
            return Promise.reject(new Error(response.data.errmsg))
        }
    }
})

export default instance