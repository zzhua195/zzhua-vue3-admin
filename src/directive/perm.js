import useMenu from '@/store/menu'
import { toRaw } from '@vue/reactivity'


export default {
    hasPerms: {
        mounted(el,binding) {
            const menuStore = useMenu()
            let perms1 = menuStore.perms

            console.log(el,binding,perms1);

            let perms2 = toRaw(perms1)
            let perms3 = JSON.parse(JSON.stringify(perms1))
            console.log(perms2.perms);
            console.log(perms3.perms);

            // 有任一指定的权限, 即可显示指定的dom, 否则移除
            if(!perms2.perms.some(p=>binding.value.includes(p))) {
                el.parentNode.removeChild(el)
            }
        },
    }
}